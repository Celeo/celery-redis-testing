from flask import Flask, render_template
from datetime import datetime, timedelta
from celery import Celery
from cel.shared import app_settings
from flask_socketio import SocketIO

import eventlet
eventlet.monkey_patch()

app = Flask(__name__)
app.config.from_pyfile('config.cfg')
for k, v in app.config.items():
    app_settings[k] = v
app.permanent_session_lifetime = timedelta(days=14)

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


socketio = SocketIO(app)


@celery.task
def background_task(url):
    print('foobar')
    with open('background_task.log', 'a') as f:
        f.write('%s\n' % datetime.now())
    print('bazpaz')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/bg')
def start_background_thread():
    print('Starting background celery task thread call ...')
    background_task.delay(app.config['SOCKETIO_REDIS_URL'])
    return 'Started'
